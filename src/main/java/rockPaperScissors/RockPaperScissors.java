package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public String computerChoice(List<String> rpsChoices){
        Random r = new Random();

        int randomitem = r.nextInt(rpsChoices.size());
        String randomElement = rpsChoices.get(randomitem);
        return randomElement;
    } 
    
    public String player() {
        String playerChoice = readInput("Your choice (Rock/Paper/Scissors)?");
        return playerChoice;
    }

    public void run() {
        // TODO: Implement Rock Paper Scissors
        System.out.println("Let's play round " + roundCounter);
        while (true) {
            String playerChoice = player();
            String computer = computerChoice(rpsChoices);
            String winner = decideWinner(playerChoice, computer);
            System.out.println("Human chose " + playerChoice + " computer chose " + computer + ". " + winner);
            System.out.println("Score: human "+ humanScore + " computer "+ computerScore);
            // kommer mer her
            String fortsette = readInput("Do you wish to continue playing? (y/n)?");
            if (fortsette.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter++;
            System.out.println("Let's play round " + roundCounter);
            continue;
        }
    }

    private String decideWinner(String player, String computer){
        String playerVictory = "Player wins!";
        String computerVictory = "Computer wins!";
        String resultat = "It's a tie!";
        if (computer.equals("rock")){
            if (player.equals("paper")){
                humanScore++;
                resultat = playerVictory;
            } else if (player.equals("scissors")){
                computerScore++;
                resultat = computerVictory;
            }
        } else if (computer.equals("paper")){
            if (player.equals("scissors")){
                humanScore++;
                resultat = playerVictory;
            } else if (player.equals("rock")){
                computerScore++;
                resultat = computerVictory;
            }
        } else if (computer.equals("scissors")){
            if (player.equals("rock")){
                humanScore++;
                resultat = playerVictory;
            } else if (player.equals("paper")){
                computerScore++;
                resultat = computerVictory;
            }
        }
        return resultat;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    

}
